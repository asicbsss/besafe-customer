package main

import (
	"flag"
	"os"

	"github.com/forstes/besafe-go/customer/pkg/store/postgres"
	grpc "github.com/forstes/besafe-go/customer/services/journey/internal/delivery/grpc/v1"
	"github.com/forstes/besafe-go/customer/services/journey/internal/repository"
	service "github.com/forstes/besafe-go/customer/services/journey/internal/usecase"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

func main() {
	dir, err := os.Getwd()
	if err != nil {
		// TODO decide which logger do we use lol
		log.Err(err)
	}

	err = godotenv.Load(dir + "/.env")
	if err != nil {
		log.Err(err).Msg("Failed to load .env file")
		os.Exit(1)
	}

	dbConnCfg := postgres.ConnectionConfig{}
	var grpcPort int

	flag.IntVar(&grpcPort, "http-port", 8001, "gRPC server port")

	flag.IntVar(&dbConnCfg.Port, "pg-port", 5432, "Postgres port")
	flag.StringVar(&dbConnCfg.Host, "pg-host", "localhost", "Postgres host")
	flag.StringVar(&dbConnCfg.User, "pg-user", os.Getenv("PG_USER"), "Postgres user")
	flag.StringVar(&dbConnCfg.Password, "pg-password", os.Getenv("PG_PASSWORD"), "Postgres password")
	flag.StringVar(&dbConnCfg.DbName, "pg-db-name", os.Getenv("PG_DB_NAME"), "Postgres DB name")
	flag.IntVar(&dbConnCfg.MaxOpenConnections, "pg-max-open-conns", 15, "Postgres max open connections")
	flag.StringVar(&dbConnCfg.MaxIdleTime, "pg-max-idle-time", "15m", "Postgres max connection idle time")
	flag.Parse()

	db, err := postgres.OpenDB(dbConnCfg)
	if err != nil {
		log.Err(err)
		os.Exit(1)
	}
	defer db.Close()

	log.Info().Msg("Connected to Postgres DB")

	journeyRepository := repository.NewJourneyRepo(db)
	journeyService := service.NewJourneyService(journeyRepository)
	grpcServer := grpc.NewGrpcServer(journeyService)
	err = grpcServer.Serve(grpcPort)
	if err != nil {
		log.Err(err).Msg("Failed to start gRPC server")
	}
}
