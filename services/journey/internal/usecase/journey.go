package service

import (
	"context"
	"math"

	"github.com/forstes/besafe-go/customer/services/journey/internal/repository"
)

type Journeys interface {
	AddJourney(ctx context.Context, input Order) error
	LoadJourney(ctx context.Context, driverID int64) error
	CompleteJourney(ctx context.Context, journeyID int64) error
	ProcessJourney(ctx context.Context, driverID int64, driverCoords Coordinates) (JourneyStage, error)
}

type Order struct {
	OrderId       int64
	UserId        int64
	DriverId      int64
	Price         float32
	StartingPoint Coordinates
	EndingPoint   Coordinates
}

type Coordinates struct {
	Latitude  float64
	Longitude float64
}

type JourneyStage int

const (
	GoingToStartPoint JourneyStage = iota
	GoingToEndPoint
	Finished
)

type activeJourney struct {
	ID         int64
	Stage      JourneyStage
	StartPoint Coordinates
	EndPoint   Coordinates
}

type journeyService struct {
	repo           repository.Journey
	activeJourneys map[int64]*activeJourney
}

func NewJourneyService(repo repository.Journey) *journeyService {
	return &journeyService{repo: repo, activeJourneys: make(map[int64]*activeJourney)}
}

// In meters
const (
	earthRadius       = 6371000
	distanceThreshold = 100
)

func calculateDistance(lat1, lon1, lat2, lon2 float64) float64 {
	// Convert degrees to radians
	lat1Rad := lat1 * math.Pi / 180
	lon1Rad := lon1 * math.Pi / 180
	lat2Rad := lat2 * math.Pi / 180
	lon2Rad := lon2 * math.Pi / 180

	// Calculate differences
	latDiff := lat2Rad - lat1Rad
	lonDiff := lon2Rad - lon1Rad

	// Apply Haversine formula
	a := math.Pow(math.Sin(latDiff/2), 2) + math.Cos(lat1Rad)*math.Cos(lat2Rad)*math.Pow(math.Sin(lonDiff/2), 2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	distance := earthRadius * c
	return distance
}

func isWithinRange(driver, endpoint Coordinates) bool {
	distance := calculateDistance(driver.Latitude, driver.Longitude, endpoint.Latitude, endpoint.Longitude)
	return distance <= distanceThreshold
}

// TODO Write tests
func (s *journeyService) ProcessJourney(ctx context.Context, driverID int64, driverCoords Coordinates) (JourneyStage, error) {
	journey := s.activeJourneys[driverID]

	if journey == nil {
		return Finished, nil
	}

	switch journey.Stage {
	case GoingToStartPoint:
		if isWithinRange(driverCoords, journey.StartPoint) {
			journey.Stage = GoingToEndPoint
		}
	case GoingToEndPoint:
		if isWithinRange(driverCoords, journey.EndPoint) {
			err := s.CompleteJourney(ctx, journey.ID)
			if err != nil {
				return journey.Stage, err
			}
			delete(s.activeJourneys, driverID)
			// TODO Send the order to payment microservice
		}
	}
	return journey.Stage, nil
}

func (s *journeyService) CompleteJourney(ctx context.Context, journeyID int64) error {
	return nil
}

func (s *journeyService) LoadJourney(ctx context.Context, driverID int64) error {
	return nil
}

func (s *journeyService) AddJourney(ctx context.Context, input Order) error {
	return nil
}
