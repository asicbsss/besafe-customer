package repository

import (
	"context"

	"github.com/forstes/besafe-go/customer/services/journey/internal/domain"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Journey interface {
	Create(ctx context.Context, user *domain.Journey) error
	GetByDriverID(ctx context.Context, id string) (*domain.Journey, error)
}

type journeyRepo struct {
	db *pgxpool.Pool
}

func NewJourneyRepo(db *pgxpool.Pool) *journeyRepo {
	return &journeyRepo{db: db}
}

func (j *journeyRepo) Create(ctx context.Context, user *domain.Journey) error {
	return nil
}

func (j *journeyRepo) GetByDriverID(ctx context.Context, id string) (*domain.Journey, error) {
	return nil, nil
}
