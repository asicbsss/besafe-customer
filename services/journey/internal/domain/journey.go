package domain

type Journey struct {
	ID        int64
	Order     Order
	Completed bool
}

type Order struct {
	OrderID       int64
	UserID        int64
	DriverID      int64
	Price         float32
	StartingPoint Location
	EndingPoint   Location
}

type Location struct {
	Latitude  float32
	Longitude float32
}
