package websocket

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type websocketServer struct {
	router http.Handler
	config ServerConfig
}

type ServerConfig struct {
	Port         int
	IdleTimeout  string
	ReadTimeout  string
	WriteTimeout string
}

func NewWebsocketServer(router http.Handler, cfg ServerConfig) *websocketServer {
	return &websocketServer{router: router, config: cfg}
}

func (s *websocketServer) serve() error {

	srv := &http.Server{
		Addr:    fmt.Sprintf("localhost:%d", s.config.Port),
		Handler: s.router,
	}

	shutdownError := make(chan error)

	go func() {
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		<-quit

		log.Print("shutting down server")

		ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
		defer cancel()

		shutdownError <- srv.Shutdown(ctx)
	}()

	log.Printf("starting server on %s", srv.Addr)
	err := srv.ListenAndServe()
	if !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	err = <-shutdownError
	if err != nil {
		return err
	}
	log.Print("stopped server")

	return nil
}
