package v1

import (
	"encoding/json"
	"log"
	"net/http"

	service "github.com/forstes/besafe-go/customer/services/journey/internal/usecase"
	"github.com/gorilla/websocket"
)

type journeyHandler struct {
	journeyService service.Journeys
	connections    map[*websocket.Conn]bool
	broadcast      chan []byte
	upgrader       websocket.Upgrader
}

func NewJourneyHandler(journeyService service.Journeys) *journeyHandler {
	return &journeyHandler{
		journeyService: journeyService,
		connections:    make(map[*websocket.Conn]bool),
		broadcast:      make(chan []byte),
		upgrader:       websocket.Upgrader{},
	}
}

type Coordinates struct {
	DriverID  int64   `json:"driver_id"`
	Latitude  float64 `json:"laittude"`
	Longitude float64 `json:"longitude"`
}

func (h *journeyHandler) HandleDriverLocation(w http.ResponseWriter, r *http.Request) {
	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}
	h.connections[conn] = true

	_, msgBytes, err := conn.ReadMessage()
	if err != nil {
		log.Fatal("failed to read ws client message")
		return
	}

	var message Coordinates

	err = json.Unmarshal(msgBytes, &message)
	if err != nil {
		log.Fatal("failed to unmarshal ws client message")
		return
	}

	driverCoords := service.Coordinates{Latitude: message.Latitude, Longitude: message.Longitude}
	h.journeyService.ProcessJourney(r.Context(), message.DriverID, driverCoords)
	// TODO handle journey stages
}
