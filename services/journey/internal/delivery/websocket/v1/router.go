package v1

import (
	"net/http"

	service "github.com/forstes/besafe-go/customer/services/journey/internal/usecase"
	"github.com/julienschmidt/httprouter"
)

type handlers struct {
	journey journeyHandler
}

func NewRouter(journeys service.Journeys) http.Handler {
	return getRoutes(handlers{journey: *NewJourneyHandler(journeys)})
}

func getRoutes(handlers handlers) http.Handler {

	router := httprouter.New()

	router.HandlerFunc(http.MethodPost, "/v1/user/register", handlers.journey.HandleDriverLocation)

	return router
}
