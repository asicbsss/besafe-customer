package grpc

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "github.com/forstes/besafe-go/customer/pkg/proto/v1/gen"
	service "github.com/forstes/besafe-go/customer/services/journey/internal/usecase"
	"google.golang.org/grpc"
)

type orderServiceServer struct {
	pb.UnimplementedOrderServiceServer
	journeyService service.Journeys
}

func NewGrpcServer(journeys service.Journeys) *orderServiceServer {
	return &orderServiceServer{journeyService: journeys}
}

func (s *orderServiceServer) Serve(port int) error {
	server := grpc.NewServer()

	pb.RegisterOrderServiceServer(server, &orderServiceServer{})

	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		return err
	}

	log.Printf("gRPC server listening on port %d", port)
	if err := server.Serve(listener); err != nil {
		return err
	}
	return nil
}

func (s *orderServiceServer) sendOrder(ctx context.Context, order *pb.Order) (*pb.SendOrderResponse, error) {
	// TODO process the received order here and return a SendOrderResponse
	fmt.Printf("Received order: %+v\n", order)

	orderInput := service.Order{
		OrderId:  order.OrderId,
		UserId:   order.UserId,
		DriverId: order.DriverId,
		Price:    order.Price,
		StartingPoint: service.Coordinates{
			Latitude:  float64(order.StartingPoint.Latitude),
			Longitude: float64(order.StartingPoint.Longitude),
		},
		EndingPoint: service.Coordinates{
			Latitude:  float64(order.EndingPoint.Latitude),
			Longitude: float64(order.EndingPoint.Longitude),
		},
	}
	s.journeyService.AddJourney(ctx, orderInput)

	response := &pb.SendOrderResponse{
		OrderId:  order.OrderId,
		Accepted: true,
	}
	return response, nil
}
