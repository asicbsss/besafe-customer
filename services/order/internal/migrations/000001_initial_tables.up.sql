CREATE TABLE IF NOT EXISTS orders (
    id bigserial PRIMARY KEY,

    client_name text NOT NULL,
	client_phone text NOT NULL,

    start_latitude numeric(9,6) NOT NULL,
    start_longitude numeric(9,6) NOT NULL,
    dest_latitude numeric(9,6) NOT NULL,
    dest_longitude numeric(9,6) NOT NULL,

    price numeric(7,2) NOT NULL,
    car_type smallint NOT NULL,
    driver_gender smallint NOT NULL,
    order_status smallint NOT NULL,

    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW()
);