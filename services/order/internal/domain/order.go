package domain

import "time"

type Order struct {
	ID                 int64
	Client             Client
	Start              Location
	Destination        Location
	Price              float64
	DriverRequirements DriverRequirements
	Status             Status
	CreatedAt          time.Time
}

type Client struct {
	Name  string
	Phone string
}

type Location struct {
	Latitude  float64
	Longitude float64
}

type DriverRequirements struct {
	CarType CarType
	Gender  Gender
}

type CarType int8

const (
	Econom CarType = iota
	Comfort
	Business
)

type Gender int8

const (
	Male Gender = iota
	Female
	Other
)

type Status int8

const (
	Pending   Status = iota // В поиске водителя
	InProcess               // В пути
	Finished                // Выполнен
	Canceled                // Отменен
)
