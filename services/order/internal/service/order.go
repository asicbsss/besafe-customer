package service

import (
	"context"
	"math"

	"github.com/forstes/besafe-go/customer/services/order/internal/domain"
	"github.com/forstes/besafe-go/customer/services/order/internal/repository"
)

type OrderInput struct {
	ClientName     string
	ClientPhone    string
	StartLatitude  float64
	StartLongitude float64
	DestLatitude   float64
	DestLongitude  float64
	Price          float64
	CarType        int8
	DriverGender   int8
	Status         int8
}

type OrderLocationsInput struct {
	StartLatitude  float64
	StartLongitude float64
	DestLatitude   float64
	DestLongitude  float64
}

type Orders interface {
	Create(context.Context, OrderInput) error
	CalculatePrice(context.Context, OrderLocationsInput) (float64, error)
}

type orderService struct {
	repo repository.Orders
}

func NewOrderService(repo repository.Orders) *orderService {
	return &orderService{repo: repo}
}

func (s *orderService) Create(ctx context.Context, input OrderInput) error {
	locations := OrderLocationsInput{
		StartLatitude:  input.StartLatitude,
		StartLongitude: input.StartLongitude,
		DestLatitude:   input.DestLatitude,
		DestLongitude:  input.DestLongitude,
	}

	price, err := s.CalculatePrice(ctx, locations)
	if err != nil {
		return err
	}

	order := domain.Order{
		Client: domain.Client{
			Name:  input.ClientName,
			Phone: input.ClientPhone},

		Start: domain.Location{
			Latitude:  input.StartLatitude,
			Longitude: input.StartLongitude},

		Destination: domain.Location{
			Latitude:  input.DestLatitude,
			Longitude: input.DestLongitude},

		Price: price,

		DriverRequirements: domain.DriverRequirements{
			CarType: domain.CarType(input.CarType),
			Gender:  domain.Gender(input.DriverGender),
		},

		Status: domain.Pending,
	}

	err = s.repo.Create(ctx, &order)
	if err != nil {
		return err
	}

	return nil
}

func (s *orderService) CalculatePrice(ctx context.Context, input OrderLocationsInput) (float64, error) {
	// TODO Validation

	lat := math.Abs(input.DestLatitude - input.StartLatitude)
	long := math.Abs(input.DestLongitude - input.StartLongitude)
	distance := math.Sqrt(math.Pow(lat, 2) + math.Pow(long, 2))

	price := distance * 100

	return price, nil
}
