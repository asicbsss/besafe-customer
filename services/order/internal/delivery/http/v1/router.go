package v1

import (
	"net/http"

	"github.com/forstes/besafe-go/customer/services/order/internal/service"
	"github.com/julienschmidt/httprouter"
)

type router struct {
	order OrderHandler
}

func NewRouter(orderService service.Orders) *router {
	return &router{order: *NewOrderHandler(orderService)}
}

func (r *router) GetRoutes() http.Handler {

	router := httprouter.New()

	router.HandlerFunc(http.MethodPost, "/v1/order/create", r.order.Create)

	return requireAuthentication(router)
}
