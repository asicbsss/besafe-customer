package v1

import (
	"net/http"
	"strings"

	"github.com/forstes/besafe-go/customer/pkg/request"
	"github.com/golang-jwt/jwt"
)

func requireAuthentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")

		headerParts := strings.Split(authHeader, " ")
		if len(headerParts) != 2 || headerParts[0] != "Bearer" {
			request.AuthenticationRequiredResponse(w, r)
			return
		}

		token := headerParts[1]
		payload := strings.Split(token, ".")[1]
		bytes, err := jwt.DecodeSegment(payload)
		if err != nil {
			request.ServerErrorResponse(w, r, err)
			return
		}
		js := string(bytes)

		r = r.WithContext(*request.ContextSetValue(r, "user", js))

		next.ServeHTTP(w, r)
	})
}
