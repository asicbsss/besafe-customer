package v1

import (
	"fmt"
	"net/http"

	"github.com/forstes/besafe-go/customer/pkg/request"
	"github.com/forstes/besafe-go/customer/services/order/internal/service"
)

type OrderHandler struct {
	orderService service.Orders
}

func NewOrderHandler(orderService service.Orders) *OrderHandler {
	return &OrderHandler{orderService: orderService}
}

type createOrderDTO struct {
	StartLatitude  float64 `json:"start_latitude"`
	StartLongitude float64 `json:"start_longitude"`
	DestLatitude   float64 `json:"dest_latitude"`
	DestLongitude  float64 `json:"dest_longitude"`
	CarType        int8    `json:"car_type"`
	DriverGender   int8    `json:"driver_gender"`
}

func (h *OrderHandler) Create(w http.ResponseWriter, r *http.Request) {
	user := request.ContextGetValue(r, "user")

	var userDto map[string]any
	request.ReadContext(user, &userDto)

	fullName := fmt.Sprintf("%s %s", userDto["firstName"], userDto["lastName"])
	phone := userDto["phone"].(string)

	var dto createOrderDTO

	if err := request.ReadJSON(w, r, &dto); err != nil {
		request.BadRequestResponse(w, r, err)
		return
	}

	input := service.OrderInput{
		ClientName:     fullName,
		ClientPhone:    phone,
		StartLatitude:  dto.StartLatitude,
		StartLongitude: dto.StartLongitude,
		DestLatitude:   dto.DestLatitude,
		DestLongitude:  dto.DestLongitude,
		CarType:        dto.CarType,
		DriverGender:   dto.DriverGender,
	}

	err := h.orderService.Create(r.Context(), input)
	if err != nil {
		request.ServerErrorResponse(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
