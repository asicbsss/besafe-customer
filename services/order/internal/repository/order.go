package repository

import (
	"context"

	"github.com/forstes/besafe-go/customer/services/order/internal/domain"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Orders interface {
	Create(ctx context.Context, order *domain.Order) error
}

type orderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) *orderRepo {
	return &orderRepo{db: db}
}

func (r *orderRepo) Create(ctx context.Context, o *domain.Order) error {
	query := `
	INSERT INTO orders (client_name, client_phone, start_latitude, start_longitude, dest_latitude, dest_longitude,
						price, car_type, driver_gender, order_status)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	RETURNING id, created_at`

	args := []any{
		o.Client.Name,
		o.Client.Phone,
		o.Start.Latitude,
		o.Start.Longitude,
		o.Destination.Latitude,
		o.Destination.Longitude,
		o.Price,
		o.DriverRequirements.CarType,
		o.DriverRequirements.Gender,
		o.Status}

	err := r.db.QueryRow(ctx, query, args...).Scan(&o.ID, &o.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}
