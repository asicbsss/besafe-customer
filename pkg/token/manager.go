package token

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
)

type TokenManager interface {
	NewToken(payload map[string]any, ttl time.Duration) (string, error)
	Parse(accessToken string) (string, error)
}

type Manager struct {
	signingKey string
}

func NewManager(signingKey string) (*Manager, error) {
	if signingKey == "" {
		return nil, errors.New("empty signing key")
	}
	return &Manager{signingKey: signingKey}, nil
}

func (m *Manager) NewToken(payload map[string]any, ttl time.Duration) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(ttl).Unix()
	for k, v := range payload {
		claims[k] = v
	}

	return token.SignedString([]byte(m.signingKey))
}

func (m *Manager) Parse(accessToken string) (string, error) {
	return "", nil
}
