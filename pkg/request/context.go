package request

import (
	"context"
	"net/http"

	"github.com/rs/zerolog/log"
)

func ContextSetValue(r *http.Request, key string, value string) *context.Context {
	ctx := context.WithValue(r.Context(), key, value)
	return &ctx
}

func ContextGetValue(r *http.Request, key string) string {
	data, ok := r.Context().Value(key).(string)
	log.Print(data)
	if !ok {
		panic("missing data value in request context")
	}

	return data
}
